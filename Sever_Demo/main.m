//
//  main.m
//  Sever_Demo
//
//  Created by zc on 13-8-16.
//  Copyright (c) 2013年 zc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
